package net.itxxw.learning.client;

import net.itxxw.framework.client.itxxwServiceList;
import net.itxxw.framework.domain.course.TeachplanMediaPub;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author：ycpan
 * @date：Created in 2021/5/19 22:49
 * @description：
 * @modified By：
 * @version: $
 */
@FeignClient(value=itxxwServiceList.ITXXW_SERVICE_SEARCH)
public interface CourseSearchClient {

    //根据课程计划id查询课程媒资
    @GetMapping("/search/course/getmedia/{teachplanId}")
    public TeachplanMediaPub getmedia(@PathVariable("teachplanId") String teachplanId);
}
