package net.itxxw.learning.controller;

import net.itxxw.api.learning.CourseLearningControllerApi;
import net.itxxw.framework.domain.learning.respones.GetMediaResult;
import net.itxxw.learning.service.LearningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author：ycpan
 * @date：Created in 2021/5/19 23:01
 * @description：
 * @modified By：
 * @version: $
 */
@RestController
@RequestMapping("/learning/course")
public class CourseLearningController implements CourseLearningControllerApi {
    @Autowired
    LearningService learningService;
    @Override
    @GetMapping("/getmedia/{courseId}/{teachplanId}")
    public GetMediaResult getMediaPlayUrl(@PathVariable String courseId, @PathVariable String teachplanId) {
        //获取课程学习地址
        return learningService.getmedia(courseId, teachplanId);
    }
}