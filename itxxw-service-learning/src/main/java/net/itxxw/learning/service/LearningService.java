package net.itxxw.learning.service;

import net.itxxw.framework.domain.course.TeachplanMediaPub;
import net.itxxw.framework.domain.learning.XcLearningCourse;
import net.itxxw.framework.domain.learning.respones.GetMediaResult;
import net.itxxw.framework.domain.learning.respones.LearningCode;
import net.itxxw.framework.domain.task.XcTask;
import net.itxxw.framework.domain.task.XcTaskHis;
import net.itxxw.framework.exception.ExceptionCast;
import net.itxxw.framework.model.response.CommonCode;
import net.itxxw.framework.model.response.ResponseResult;
import net.itxxw.learning.client.CourseSearchClient;
import net.itxxw.learning.dao.XcLearningCourseRepository;
import net.itxxw.learning.dao.XcTaskHisRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;

/**
 * @author：ycpan
 * @date：Created in 2021/5/19 22:52
 * @description：
 * @modified By：
 * @version: $
 */
@Service
public class LearningService {

    //远程调用搜索服务的接口
    @Autowired
    CourseSearchClient courseSearchClient;
    @Autowired
    XcTaskHisRepository xcTaskHisRepository;
    @Autowired
    XcLearningCourseRepository xcLearningCourseRepository;

    //获取课程学习地址（视频播放地址）
    public GetMediaResult getmedia(String courseId, String teachplanId) {
        //校验学生的学生权限...

        //远程调用搜索服务查询课程计划所对应的课程媒资信息
        TeachplanMediaPub teachplanMediaPub = courseSearchClient.getmedia(teachplanId);
        //播放地址为空，
        if(teachplanMediaPub == null || StringUtils.isEmpty(teachplanMediaPub.getMediaUrl())){
            //获取学习地址错误
            ExceptionCast.cast(LearningCode.LEARNING_GET_MEDIA_ERROR);
        }
        //不为空，返回成功
        return new GetMediaResult(CommonCode.SUCCESS,teachplanMediaPub.getMediaUrl());
    }

    /**
     * 为用户添加选课信息的实现
     * @param userId 用户id
     * @param courseId 课程id
     * @param valid 课程有效性,标识该课程是否已经到期
     * @param startTime 选课开始时间
     * @param endTime 结束时间
     * @param xcTask 具体课程信息
     * @return
     */
    @Transactional
    public ResponseResult addChooseCourse(String userId, String courseId, String valid, Date startTime, Date endTime, XcTask xcTask) {
        //空指针判断
        if (StringUtils.isEmpty(courseId)) {
            ExceptionCast.cast(LearningCode.CHOOSECOURSE_COURSEID_ISNULL);
        }
        if (StringUtils.isEmpty(userId)) {
            ExceptionCast.cast(LearningCode.CHOOSECOURSE_USERID_ISNULL);
        }
        if(xcTask == null || StringUtils.isEmpty(xcTask.getId())){
            ExceptionCast.cast(LearningCode.CHOOSECOURSE_TASKID_ISNULL);
        }
        //如果查询到任务记录,则表示该任务已经完成,返回成功标识
        Optional<XcTaskHis> optional = xcTaskHisRepository.findById(xcTask.getId());
        if(optional.isPresent()){
            return new ResponseResult(CommonCode.SUCCESS);
        }

        //查询是否已经添加了选课
        XcLearningCourse byUserIdAndCourseId = xcLearningCourseRepository.findXcLearningCourseByUserIdAndCourseId(userId, courseId);
        if ((byUserIdAndCourseId == null)) {
            byUserIdAndCourseId = new XcLearningCourse();
            byUserIdAndCourseId.setUserId(userId);
            byUserIdAndCourseId.setCourseId(courseId);
            byUserIdAndCourseId.setValid(valid);
            byUserIdAndCourseId.setStartTime(startTime);
            byUserIdAndCourseId.setEndTime(endTime);
            byUserIdAndCourseId.setStatus("501001");
        }else{
            //该课程如果已经选过,则更新该课程的信息
            byUserIdAndCourseId.setValid(valid);
            byUserIdAndCourseId.setStartTime(startTime);
            byUserIdAndCourseId.setEndTime(endTime);
            byUserIdAndCourseId.setStatus("501001");
        }

        xcLearningCourseRepository.save(byUserIdAndCourseId);
        //向历史任务表播入记录
        Optional<XcTaskHis> optionalXcTaskHis = xcTaskHisRepository.findById(xcTask.getId());
        if(!optionalXcTaskHis.isPresent()){
            //添加历史任务
            XcTaskHis xcTaskHis = new XcTaskHis();
            BeanUtils.copyProperties(xcTask,xcTaskHis);
            xcTaskHisRepository.save(xcTaskHis);
        }

        return new ResponseResult(CommonCode.SUCCESS);
    }

}
