package net.itxxw.learning.dao;

import net.itxxw.framework.domain.learning.XcLearningCourse;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author：ycpan
 * @date：Created in 2021/6/11 21:05
 * @description：学生选课Dao
 * @modified By：
 * @version: $
 */
public interface XcLearningCourseRepository extends JpaRepository<XcLearningCourse, String> {
    //根据用户和课程查询选课记录，用于判断是否添加选课
    XcLearningCourse findXcLearningCourseByUserIdAndCourseId(String userId, String courseId);
}