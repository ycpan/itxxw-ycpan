package net.itxxw.learning.dao;

import net.itxxw.framework.domain.task.XcTaskHis;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author：ycpan
 * @date：Created in 2021/6/11 21:06
 * @description：历史任务Dao：
 * @modified By：
 * @version: $
 */
public interface XcTaskHisRepository extends JpaRepository<XcTaskHis,String> {
}