package net.itxxw.manage_media.controller;

import net.itxxw.api.media.MediaFileControllerApi;
import net.itxxw.framework.domain.media.request.QueryMediaFileRequest;
import net.itxxw.framework.model.response.QueryResponseResult;
import net.itxxw.manage_media.service.MediaFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author：ycpan
 * @date：Created in 2021/5/2 16:40
 * @description：
 * @modified By：
 * @version: $
 */

@RestController
@RequestMapping("/media/file")
public class MediaFileController implements MediaFileControllerApi {

    @Autowired
    MediaFileService mediaFileService;

    @GetMapping("/list/{page}/{size}")
    @Override
    public QueryResponseResult findList(@PathVariable("page") int page, @PathVariable("size") int size, QueryMediaFileRequest queryMediaFileRequest) {
       //媒资文件信息查询
        return mediaFileService.findList(page,size,queryMediaFileRequest);
    }


}
