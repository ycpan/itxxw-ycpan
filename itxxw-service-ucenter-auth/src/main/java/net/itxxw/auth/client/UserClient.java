package net.itxxw.auth.client;

import net.itxxw.framework.client.itxxwServiceList;
import net.itxxw.framework.domain.ucenter.ext.XcUserExt;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author：ycpan
 * @date：Created in 2021/5/29 18:44
 * @description：
 * @modified By：
 * @version: $
 */
@FeignClient(value = itxxwServiceList.ITXXW_SERVICE_UCENTER)
public interface UserClient {
    @GetMapping("/ucenter/getuserext")
    public XcUserExt getUserext(@RequestParam("username") String username);
}