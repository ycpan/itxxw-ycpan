package net.itxxw.manage_cms.controller;
import net.itxxw.api.cms.CmsPageControllerApi;
import net.itxxw.framework.domain.cms.CmsPage;
import net.itxxw.framework.domain.cms.request.QueryPageRequest;
import net.itxxw.framework.domain.cms.response.CmsPageResult;
import net.itxxw.framework.domain.cms.response.CmsPostPageResult;
import net.itxxw.framework.model.response.QueryResponseResult;
import net.itxxw.framework.model.response.ResponseResult;
import net.itxxw.manage_cms.service.PageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;




@RestController
@RequestMapping("/cms/page")
public class CmsPageController implements CmsPageControllerApi {
     @Autowired
     PageService pageService;
     @Override
     @GetMapping("/list/{page}/{size}")
     public QueryResponseResult findCmsPageList(@PathVariable("page") int page, @PathVariable("size")
             int size, QueryPageRequest queryPageRequest) {
         return pageService.findCmsPageList(page,size,queryPageRequest);
     }

     //添加页面
     @Override
     @PostMapping("/add")
     public CmsPageResult addCmsPage(@RequestBody CmsPage cmsPage) {
          return pageService.addCmsPage(cmsPage);
     }

     //根据id查询页面
     @Override
     @GetMapping("/get/{id}")
     public CmsPage findCmsPageById(@PathVariable("id") String id) {
          return pageService.findCmsPageById(id);
     }

     //修改页面信息
     @Override
     @PutMapping("/edit/{id}")//这里使用put方法，http 方法中put表示更新
     public CmsPageResult updateCmsPage(@PathVariable("id") String id, @RequestBody CmsPage cmsPage) {
          return pageService.updateCmsPage(id,cmsPage);
     }

     //页面保存
     @Override
     @PostMapping("/save")
     public CmsPageResult save(@RequestBody CmsPage cmsPage) {
          return pageService.saveCmsPage(cmsPage);
     }

     //发布页面
     @Override
     @PostMapping("/postPage/{pageId}")
     public ResponseResult post(@PathVariable("pageId") String pageId) {
          return pageService.post(pageId);
     }

     //发布页面的方法
     @Override
     @PostMapping("/postPageQuick")
     public CmsPostPageResult postPageQuick(@RequestBody CmsPage cmsPage){
          CmsPostPageResult cmsPostPageResult = pageService.postPageQuick(cmsPage);

          return cmsPostPageResult;
     }


     @DeleteMapping("/del/{id}")//使用http的delete方法完成删除操作
      public ResponseResult delete(@PathVariable("id") String id){
          return pageService.delete(id);
     }
}