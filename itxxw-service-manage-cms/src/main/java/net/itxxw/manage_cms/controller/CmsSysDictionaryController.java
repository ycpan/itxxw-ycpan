package net.itxxw.manage_cms.controller;

import net.itxxw.api.cms.CmsSysDictionaryControllerApi;
import net.itxxw.framework.domain.system.SysDictionary;
import net.itxxw.manage_cms.service.PageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author：ycpan
 * @date：Created in 2021/3/8 22:53
 * @description：字典匹配控制器
 * @modified By：
 * @version: 1$
 */
@RestController
@RequestMapping("/sys")
public class CmsSysDictionaryController implements CmsSysDictionaryControllerApi {
    @Autowired
    PageService pageService;

    @GetMapping("/dictionary/get/{dtype}")
    public SysDictionary findSysDictionary(@PathVariable String dtype){
        SysDictionary sysDictionary = pageService.findSysDictionary(dtype);
        return sysDictionary;
    }
}
