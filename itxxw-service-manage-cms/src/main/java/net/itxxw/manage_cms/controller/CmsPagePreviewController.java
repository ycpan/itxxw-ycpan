package net.itxxw.manage_cms.controller;

import net.itxxw.framework.web.BaseController;
import net.itxxw.manage_cms.service.CmsConfigService;
import net.itxxw.manage_cms.service.PageService;
import freemarker.template.TemplateException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletOutputStream;
import java.io.IOException;
//由于要向浏览器输出，html静态页面，是字符串，而不是json数据格式，所以用@controller
@Controller
public class CmsPagePreviewController extends BaseController {
    @Autowired
    PageService pageService;
    @Autowired
    CmsConfigService cmsConfigService;
    //页面预览
    //接收到页面id
    @RequestMapping(value="/cms/preview/{pageId}",method = RequestMethod.GET)
    public void preview(@PathVariable("pageId")String pageId) throws IOException, TemplateException {
        //执行静态化
        String pageHtml = cmsConfigService.getPageHtml(pageId);
        //通过Response对象将内容输出
        if(StringUtils.isNotEmpty(pageHtml)){
            try {
                ServletOutputStream outputStream = response.getOutputStream();
                //页面要用SSI,组件，就要指定响应的消息头为html
                response.setHeader("Content-type","text/html;charset=utf-8");
                outputStream.write(pageHtml.getBytes("UTF-8"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
