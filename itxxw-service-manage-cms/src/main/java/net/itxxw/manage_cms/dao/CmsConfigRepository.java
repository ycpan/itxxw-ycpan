package net.itxxw.manage_cms.dao;


import net.itxxw.framework.domain.cms.CmsConfig;
import org.springframework.data.mongodb.repository.MongoRepository;

//获取数据模型dao
public interface CmsConfigRepository extends MongoRepository<CmsConfig,String> {
}
