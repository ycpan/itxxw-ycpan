package net.itxxw.manage_cms.dao;

import net.itxxw.framework.domain.system.SysDictionary;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author：ycpan
 * @date：Created in 2021/3/8 22:39
 * @description：数据字典dao
 * @modified By：
 * @version: 1$
 */
public interface CmsSysDictionaryRespository extends MongoRepository<SysDictionary,String> {
    public SysDictionary findByDType(String type);
}
