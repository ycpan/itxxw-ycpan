package net.itxxw.manage_cms.dao;

import net.itxxw.framework.domain.cms.CmsSite;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author：ycpan
 * @date：Created in 2021/4/1 0:25
 * @description：
 * @modified By：
 * @version: $
 */
//接口中需要获取站点的信息（站点域名、站点访问路径等）
public interface CmsSiteRepository extends MongoRepository<CmsSite,String> {
}
