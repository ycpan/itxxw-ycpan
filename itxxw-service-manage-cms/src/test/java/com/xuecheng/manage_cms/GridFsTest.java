package net.itxxw.manage_cms;

import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSDownloadStream;
import com.mongodb.client.gridfs.model.GridFSFile;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.apache.commons.io.IOUtils;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

@SpringBootTest
@RunWith(SpringRunner.class)
public class GridFsTest {
    @Autowired
    GridFsTemplate gridFsTemplate;
    @Autowired
    GridFSBucket gridFSBucket;

    //存文件
    @Test
    public void testGridFsTemplate() throws FileNotFoundException {
     //定义file
        File file=new File("D:\\temporary\\course.ftl");
        //定义FileInputStream
        FileInputStream fileInputStream =new FileInputStream(file);
        ObjectId objectId = gridFsTemplate.store(fileInputStream, "course.ftl");
        System.out.println(objectId);
    }

    @Test
    //存模板文件
    public void cun() {
        //要存文件的绝对路径，
        File file = new File("D:\\temporary\\course.ftl");
        try {
            //把绝对路径，塞进输入流
            FileInputStream inputStream = new FileInputStream(file);
            //通过store方法，把绝对路径中的，具体文件，存进GridFS库中，并且返回一个存入文件的id
            ObjectId objectId = gridFsTemplate.store(inputStream, "course.ftl");//获取id 6061e47f50f03a0d8cb08a5d  测试页面id:4028858162e0bc0a0162e0bfdf1a0000
            System.err.println(objectId);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }



    @Test
           public void queryFile() throws IOException {
            //根据文件id查询文件
           GridFSFile gridFSFile = gridFsTemplate.findOne(Query.query(Criteria.where("_id").is("5fbe0a29dcd36814c00b4e92")));

           //打开一个下载流对象
           GridFSDownloadStream gridFSDownloadStream=gridFSBucket.openDownloadStream(gridFSFile.getObjectId());
           //创建GridFsResource对象，获取流
           GridFsResource gridFsResource=new GridFsResource(gridFSFile,gridFSDownloadStream);
           //从流中取数据
           String content= IOUtils.toString(gridFsResource.getInputStream(), "UTF-8");
           System.out.println(content);
       }

}
