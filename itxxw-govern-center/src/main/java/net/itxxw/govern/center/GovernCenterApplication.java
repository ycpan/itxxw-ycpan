package net.itxxw.govern.center;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author：ycpan
 * @date：Created in 2021/3/26 13:47
 * @description：1
 * @modified By：
 * @version: 1$
 */

@EnableEurekaServer //标识这个服务，为注册中心的服务器
@SpringBootApplication
public class GovernCenterApplication {
    public static void main(String[] args) {
        SpringApplication.run(GovernCenterApplication.class,args);
    }
}
