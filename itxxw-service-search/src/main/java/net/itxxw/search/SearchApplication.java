package net.itxxw.search;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@EnableDiscoveryClient
@SpringBootApplication
@EntityScan("net.itxxw.framework.domain.search")//扫描实体类
@ComponentScan(basePackages={"net.itxxw.api"})//扫描接口
@ComponentScan(basePackages={"net.itxxw.search"})//扫描本项目下的所有类
@ComponentScan(basePackages={"net.itxxw.framework"})//扫描common下的所有类
public class SearchApplication{

    public static void main(String[] args) throws Exception {
        SpringApplication.run(SearchApplication.class, args);
    }

}
