package net.itxxw.search.controller;

import net.itxxw.api.search.EsCourseControllerApi;
import net.itxxw.framework.domain.course.CoursePub;
import net.itxxw.framework.domain.course.TeachplanMediaPub;
import net.itxxw.framework.domain.search.CourseSearchParam;
import net.itxxw.framework.model.response.QueryResponseResult;
import net.itxxw.framework.model.response.QueryResult;
import net.itxxw.search.service.EsCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/search/course")
public class EsCourseController implements EsCourseControllerApi {
    @Autowired
    EsCourseService esCourseService;

    /***
     * 课程索引搜索search
     * @param page  当前页码
     * @param size  显示条数
     * @param courseSearchParam 综合查询条件，把所有查询条件，封装为一个条件查询的对象
     * @return  分页索引表的集合对象
     */
    @Override
    @GetMapping(value="/list/{page}/{size}")
    public QueryResponseResult<CoursePub> list(@PathVariable("page") int page,
                                               @PathVariable("size") int size,
                                               CourseSearchParam courseSearchParam) {
        return esCourseService.list(page,size,courseSearchParam);
    }


    /**
     * 根据id搜索课程发布信息
     * @param id 课程id
     * @return JSON数据
     */
    @Override
    @GetMapping("/getdetail/{id}")
    public Map<String, CoursePub> getdetail(@PathVariable("id")String id) {
        return esCourseService.getdetail(id);
    }


    /**
     * 根据课程计划id搜索发布后的媒资信息
     * @param teachplanId
     * @return
     */
    @Override
    @GetMapping(value="/getmedia/{teachplanId}")
    public TeachplanMediaPub getmedia(@PathVariable("teachplanId")  String teachplanId) {
        //为了service的拓展性,所以我们service接收的是数组作为参数,以便后续开发查询多个ID的接口
        String[] teachplanIds = new String[]{teachplanId};
        //通过service查询ES获取课程媒资信息
        QueryResponseResult<TeachplanMediaPub> mediaPubQueryResponseResult = esCourseService.getmedia(teachplanIds);
        QueryResult<TeachplanMediaPub> queryResult = mediaPubQueryResponseResult.getQueryResult();
        if(queryResult!=null&& queryResult.getList()!=null && queryResult.getList().size()>0){
            //返回课程计划对应课程媒资
            return queryResult.getList().get(0);
        }
        return new TeachplanMediaPub();
    }


}

