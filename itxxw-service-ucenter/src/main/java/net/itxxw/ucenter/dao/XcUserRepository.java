package net.itxxw.ucenter.dao;

import net.itxxw.framework.domain.ucenter.XcUser;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author：ycpan
 * @date：Created in 2021/5/29 17:52
 * @description：
 * @modified By：
 * @version: $
 */
public interface XcUserRepository extends JpaRepository<XcUser,String> {
    XcUser findXcUserByUsername(String username);
}
