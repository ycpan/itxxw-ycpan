package net.itxxw.ucenter.dao;

import net.itxxw.framework.domain.ucenter.XcCompanyUser;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author：ycpan
 * @date：Created in 2021/5/29 17:56
 * @description：
 * @modified By：
 * @version: $
 */
public interface XcCompanyUserRepository extends JpaRepository<XcCompanyUser,String> {
    //根据用户id查询所属企业id
    XcCompanyUser findByUserId(String userId);
}
