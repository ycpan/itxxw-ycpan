package net.itxxw.ucenter.dao;

import net.itxxw.framework.domain.ucenter.XcMenu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author：ycpan
 * @date：Created in 2021/6/4 18:13
 * @description：实现根据用户id查询权限。
 * @modified By：
 * @version: $
 */
@Mapper
public interface XcMenuMapper {
    public List<XcMenu> selectPermissionByUserId(String userid);
}
