package net.itxxw.ucenter.service;

import net.itxxw.framework.domain.ucenter.XcCompanyUser;
import net.itxxw.framework.domain.ucenter.XcMenu;
import net.itxxw.framework.domain.ucenter.XcUser;
import net.itxxw.framework.domain.ucenter.ext.XcUserExt;
import net.itxxw.ucenter.dao.XcCompanyUserRepository;
import net.itxxw.ucenter.dao.XcMenuMapper;
import net.itxxw.ucenter.dao.XcUserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author：ycpan
 * @date：Created in 2021/5/29 18:00
 * @description：
 * @modified By：
 * @version: $
 */
@Service
public class UserService {
    @Autowired
    private XcUserRepository xcUserRepository;

    @Autowired
    private XcCompanyUserRepository xcCompanyUserRepository;
    @Autowired
    private XcMenuMapper xcMenuMapper;

    //根据用户账号查询用户信息
    public XcUser findXcUserByUsername(String username) {
        return xcUserRepository.findXcUserByUsername(username);
    }

    //根据账号查询用户的信息，返回用户扩展信息
    public XcUserExt getUserExt(String username) {
        XcUser xcUser = this.findXcUserByUsername(username);
        if (xcUser == null) {
            return null;
        }


        XcUserExt xcUserExt = new XcUserExt();
        BeanUtils.copyProperties(xcUser, xcUserExt);

        //根据用户id查询用户所属公司
        String xcUserId = xcUser.getId();
        XcCompanyUser xcCompanyUser = xcCompanyUserRepository.findByUserId(xcUserId);
        if (xcCompanyUser != null) {
            String companyId = xcCompanyUser.getCompanyId();
            xcUserExt.setCompanyId(companyId);
        }
        //获取用户的所有权限
        List<XcMenu> xcMenus = xcMenuMapper.selectPermissionByUserId(xcUserId);
        xcUserExt.setPermissions(xcMenus);

        //返回XcUserExt对象
        return xcUserExt;

    }
}