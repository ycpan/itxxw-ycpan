package net.itxxw.ucenter.controller;

import net.itxxw.api.ucenter.UcenterControllerApi;
import net.itxxw.framework.domain.ucenter.ext.XcUserExt;
import net.itxxw.ucenter.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author：ycpan
 * @date：Created in 2021/5/29 18:14
 * @description：
 * @modified By：
 * @version: $
 */
@RestController
@RequestMapping("/ucenter")
public class UcenterController implements UcenterControllerApi {
    @Autowired
    UserService userService;
    @Override
    @GetMapping("/getuserext")
    public XcUserExt getUserext(@RequestParam("username") String username) {
        XcUserExt xcUser = userService.getUserExt(username);
        return xcUser;
    }
}