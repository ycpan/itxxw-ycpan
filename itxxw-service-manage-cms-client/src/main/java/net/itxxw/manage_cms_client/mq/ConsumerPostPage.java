package net.itxxw.manage_cms_client.mq;

import com.alibaba.fastjson.JSON;
import net.itxxw.framework.domain.cms.CmsPage;
import net.itxxw.manage_cms_client.dao.CmsPageRepository;
import net.itxxw.manage_cms_client.service.PageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author：ycpan
 * @date：Created in 2020/12/3 20:12
 * @description：监听MQ，接收页面发布消息
 * @modified By：
 * @version: 1$
 */
@Component
public class ConsumerPostPage {
    private static final Logger LOGGER= LoggerFactory.getLogger(ConsumerPostPage.class);

    @Autowired
    CmsPageRepository cmsPageRepository;
    @Autowired
    PageService pageService;
    @RabbitListener(queues = {"${xuecheng.mq.queue}"})//监听队列
    public void postPage(String msg){
       //解析消息
        Map map = JSON.parseObject(msg, Map.class);
        LOGGER.info("receive cms post page:{}", msg.toString());
        //得到消息的页面id
        String pageId = (String) map.get("pageId");
        //做一个判断，校验页面是否合法
        CmsPage cmsPage = pageService.findCmsPageById(pageId);
        if(cmsPage==null){
            LOGGER.error("receive postpage msg,cmsPage is null,pageId:{}",pageId);
            return;
        }
        //调用service方法将页面从GridFs中下载到服务器
        pageService.savePageToServerPath(pageId);

    }
}
