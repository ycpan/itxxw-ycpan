package net.itxxw.manage_cms_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author：ycpan
 * @date：Created in 2020/11/30 20:57
 * @description：1
 * @modified By：
 * @version: 1$
 */
@EnableDiscoveryClient
@SpringBootApplication
@EntityScan("net.itxxw.framework.domain.cms")//扫描实体类
@ComponentScan(basePackages={"net.itxxw.framework"})//扫描common下的所有类
@ComponentScan(basePackages={"net.itxxw.manage_cms_client"})
public class ManageCmsClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(ManageCmsClientApplication.class, args);
    }
}
