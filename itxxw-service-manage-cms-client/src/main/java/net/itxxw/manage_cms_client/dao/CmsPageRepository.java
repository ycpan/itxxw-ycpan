package net.itxxw.manage_cms_client.dao;

import net.itxxw.framework.domain.cms.CmsPage;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface CmsPageRepository extends MongoRepository<CmsPage,String> {
    CmsPage findByPageNameAndSiteIdAndPageWebPath(String pageName, String siteId, String pageWebPath);
}
