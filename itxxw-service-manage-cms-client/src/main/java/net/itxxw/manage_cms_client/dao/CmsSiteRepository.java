package net.itxxw.manage_cms_client.dao;


import net.itxxw.framework.domain.cms.CmsSite;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface CmsSiteRepository extends MongoRepository<CmsSite,String> {
}
