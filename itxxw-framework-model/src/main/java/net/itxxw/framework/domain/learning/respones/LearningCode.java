package net.itxxw.framework.domain.learning.respones;

import net.itxxw.framework.model.response.ResultCode;
import lombok.ToString;

/**
 * @author：ycpan
 * @date：Created in 2021/5/19 22:56
 * @description：
 * @modified By：
 * @version: $
 */
@ToString
public enum LearningCode implements ResultCode {
    LEARNING_GET_MEDIA_ERROR(false,23001,"学习中心获取媒资信息错误！"),
    CHOOSECOURSE_COURSEID_ISNULL(false,23002,"学习中心选课课程ID为空！"),
    CHOOSECOURSE_USERID_ISNULL(false,23003,"学习中心选课用户ID为空！"),
    CHOOSECOURSE_TASKID_ISNULL(false,23004,"学习中心选课任务ID为空！");
    //操作代码
    boolean success;
    //操作代码
    int code;
    //提示信息
    String message;
    private LearningCode(boolean success, int code, String message){
        this.success = success;
        this.code = code;
        this.message = message;
    }

    @Override
    public boolean success() {
        return success;
    }

    @Override
    public int code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }
}