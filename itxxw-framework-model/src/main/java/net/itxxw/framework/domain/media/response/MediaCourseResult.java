package net.itxxw.framework.domain.media.response;

import net.itxxw.framework.domain.media.MediaFile;
import net.itxxw.framework.domain.media.MediaVideoCourse;
import net.itxxw.framework.model.response.ResponseResult;
import net.itxxw.framework.model.response.ResultCode;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by admin on 2018/3/5.
 */
@Data
@ToString
@NoArgsConstructor
public class MediaCourseResult extends ResponseResult {
    public MediaCourseResult(ResultCode resultCode, MediaVideoCourse mediaVideoCourse) {
        super(resultCode);
        this.mediaVideoCourse = mediaVideoCourse;
    }

    MediaFile mediaVideo;
    MediaVideoCourse mediaVideoCourse;
}
