package net.itxxw.framework.domain.course.response;

import lombok.Data;
import lombok.ToString;
import net.itxxw.framework.model.response.ResponseResult;
import net.itxxw.framework.model.response.ResultCode;

/**
 * Created by mrt on 2018/3/20.
 */
@Data
@ToString
public class AddCourseResult extends ResponseResult {
    public AddCourseResult(ResultCode resultCode, String courseid) {
        super(resultCode);
        this.courseid = courseid;
    }
    private String courseid;

}
