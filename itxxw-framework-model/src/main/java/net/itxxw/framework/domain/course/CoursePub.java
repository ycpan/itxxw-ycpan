package net.itxxw.framework.domain.course;

import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by admin on 2018/2/10.
 * 课程信息分布在 course_base、course_pic 等不同的表中。
 * 课程发布成功为了方便进行索引将这几张表的数据合并在一张表中，作为课程发布信息。
 * 创建 course_pub 表
 */
@Data
@ToString
@Entity
@Table(name="course_pub")
@GenericGenerator(name = "jpa-assigned", strategy = "assigned")
public class CoursePub implements Serializable {
    private static final long serialVersionUID = -916357110051689487L;//该变量的值将被JVM 产生 和用于序列化和对象的反序列化。
    // 序列化运行联营每个序列化类版本号，被称为的serialVersionUID，这是反序列化过程中用于验证序列化对象的发送者和接收者加载类是关于序列化兼容的对象。
    // 如果接收器已经装载一个类具有不同的serialVersionUID比相应的发送者的类的对象，然后反序列化将导致InvalidClassException。
    // 可序列化类可以通过声明名为“serialVersionUID的”必须是静态的，最后一个字段显式声明long类型自身的serialVersionUID，和。

    @Id
    @GeneratedValue(generator = "jpa-assigned")
    @Column(length = 32)
    private String id;
    private String name;
    private String users;
    private String mt;
    private String st;
    private String grade;
    private String studymodel;
    private String teachmode;
    private String description;
    private String pic;//图片
    private Date timestamp;//时间戳
    private String charge;
    private String valid;
    private String qq;
    private double price;
    private double price_old;
    private String expires;
    private String teachplan;//课程计划
    @Column(name="pub_time")
    private String pubTime;//课程发布时间
}
