package net.itxxw.framework.domain.course.ext;

import lombok.Data;
import lombok.ToString;
import net.itxxw.framework.domain.course.CourseBase;

/**
 * Created by admin on 2018/2/10.
 */
@Data
@ToString
public class CourseInfo extends CourseBase {

    //课程图片
    private String pic;

}
