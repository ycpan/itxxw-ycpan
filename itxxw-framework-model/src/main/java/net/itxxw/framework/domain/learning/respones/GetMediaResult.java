package net.itxxw.framework.domain.learning.respones;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.itxxw.framework.model.response.ResponseResult;
import net.itxxw.framework.model.response.ResultCode;

/**
 * @author：ycpan
 * @date：Created in 2021/5/19 11:19
 * @description：
 * @modified By：
 * @version: $
 */
@Data
@ToString
@NoArgsConstructor
public class GetMediaResult extends ResponseResult {
    //视频播放地址
    String fileUrl;
    public GetMediaResult(ResultCode resultCode, String fileUrl){
        super(resultCode);
        this.fileUrl = fileUrl;
    }
}
