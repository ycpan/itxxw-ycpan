package net.itxxw.framework.domain.cms.response;

import net.itxxw.framework.model.response.ResponseResult;
import net.itxxw.framework.model.response.ResultCode;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author：ycpan
 * @date：Created in 2021/4/1 0:19
 * @description：
 * @modified By：
 * @version: $
 */
@Data
@ToString
//Feign,远程调用，需要无参构造。注意
@NoArgsConstructor
public class CmsPostPageResult extends ResponseResult {
    String postUrl;//页面发布的url，必须得到页面id才可以拼装
    public CmsPostPageResult(ResultCode resultCode, String postUrl) {
        super(resultCode);
        //页面发布成功cms返回页面的url=cmsSite.siteDomain+cmsSite.siteWebPath+ cmsPage.pageWebPath + cmsPage.pageName
        //就是：站点的的域名+站点绝对路径+页面路径+页面名称
        this.postUrl= postUrl;
    }
}
