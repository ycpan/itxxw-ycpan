package net.itxxw.framework.domain.course.response;


import lombok.Data;
import lombok.ToString;
import net.itxxw.framework.model.response.ResponseResult;
import net.itxxw.framework.model.response.ResultCode;

/**
 * Created by mrt on 2018/3/20.
 */
@Data
@ToString
public class DeleteCourseResult extends ResponseResult {
    public DeleteCourseResult(ResultCode resultCode, String courseId) {
        super(resultCode);
        this.courseid = courseid;
    }
    private String courseid;

}
