package net.itxxw.framework.domain.course.ext;

import net.itxxw.framework.domain.course.CoursePic;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.itxxw.framework.domain.course.CourseBase;
import net.itxxw.framework.domain.course.CourseMarket;

/**
 * @author：ycpan
 * @date：Created in 2021/3/26 21:59
 * @description：
 * @modified By：
 * @version: $
 */
@Data
//生成无参构造
@NoArgsConstructor
@ToString
public class CourseView implements java.io.Serializable {
    private CourseBase courseBase;//基础信息
    private CoursePic coursePic;//课程图片
    private CourseMarket courseMarket;//课程营销
    private TeachplanNode teachplanNode;//教学计划
}
