package net.itxxw.api.search;
import net.itxxw.framework.domain.course.CoursePub;
import net.itxxw.framework.domain.course.TeachplanMediaPub;
import net.itxxw.framework.domain.search.CourseSearchParam;
import net.itxxw.framework.model.response.QueryResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.Map;


@Api(value = "课程搜索",description = "课程搜索",tags = {"课程搜索"})
public interface EsCourseControllerApi {
    //搜索课程信息
    @ApiOperation("课程综合搜索")
    public QueryResponseResult<CoursePub> list(int page, int size, CourseSearchParam courseSearchParam);

    @ApiOperation("根据id搜索课程发布信息")
    public Map<String,CoursePub> getdetail(String id);

    @ApiOperation("根据课程计划查询媒资信息")
    public TeachplanMediaPub getmedia(String teachplanId);


}
