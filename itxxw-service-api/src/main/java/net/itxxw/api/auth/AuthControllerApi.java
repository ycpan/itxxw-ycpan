package net.itxxw.api.auth;

import net.itxxw.framework.domain.ucenter.request.LoginRequest;
import net.itxxw.framework.domain.ucenter.response.JwtResult;
import net.itxxw.framework.domain.ucenter.response.LoginResult;
import net.itxxw.framework.model.response.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author：ycpan
 * @date：Created in 2021/5/25 18:38
 * @description：
 * @modified By：
 * @version: $
 */
@Api(value = "用户认证",description = "用户认证接口")
public interface AuthControllerApi {
    @ApiOperation("登录")
    public LoginResult login(LoginRequest loginRequest);
    @ApiOperation("退出")
    public ResponseResult logout();
    @ApiOperation("查询userjwt令牌")
    public JwtResult userjwt();

}