package net.itxxw.api.filesystem;

import net.itxxw.framework.domain.filesystem.response.UploadFileResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author：ycpan
 * @date：Created in 2021/3/25 22:08
 * @description： 文件系统接口
 * @modified By：
 * @version: 1$
 */
@Api(value = "文件系统管理接口",description = "文件管理接口，提供文件的增/删/查/改")
public interface FileSystemControllerApi {
//上传文件
     @ApiOperation("上传文件接口")
    public UploadFileResult upload(
            MultipartFile multipartFile,
            String filetag,
            String businesskey,
            String metadata
    );
}
