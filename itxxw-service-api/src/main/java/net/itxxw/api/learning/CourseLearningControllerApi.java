package net.itxxw.api.learning;

import net.itxxw.framework.domain.learning.respones.GetMediaResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author：ycpan
 * @date：Created in 2021/5/19 22:25
 * @description：
 * @modified By：
 * @version: $
 */

@Api(value = "录播课程学习管理",description = "录播课程学习管理")
public interface CourseLearningControllerApi {
    @ApiOperation("获取课程学习地址")
    public GetMediaResult getMediaPlayUrl(String courseId, String teachplanId);
}
