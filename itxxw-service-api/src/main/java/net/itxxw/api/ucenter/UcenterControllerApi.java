package net.itxxw.api.ucenter;

import net.itxxw.framework.domain.ucenter.ext.XcUserExt;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author：ycpan
 * @date：Created in 2021/5/29 17:46
 * @description：
 * @modified By：
 * @version: $
 */
@Api(value = "用户中心",description = "用户中心管理")
public interface UcenterControllerApi {
    public XcUserExt getUserext(String username);


}