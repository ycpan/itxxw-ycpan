package net.itxxw.api.category;

import net.itxxw.framework.domain.course.ext.CategoryNode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author：ycpan
 * @date：Created in 2021/3/8 21:05
 * @description：课程分类接口
 * @modified By：
 * @version: 1$
 */
@Api(value = "课程分类接口",description = "课程的分类，增删改查")
public interface CategoryControllerApi {
    //查询课程分类信息
    @ApiOperation("查询课程分类")
    public CategoryNode findCategoryList();
}
