package net.itxxw.api.media;

import net.itxxw.framework.domain.media.request.QueryMediaFileRequest;
import net.itxxw.framework.model.response.QueryResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author：ycpan
 * @date：Created in 2021/5/2 13:27
 * @description：
 * @modified By：
 * @version: $
 */
@Api(value = "媒体文件管理",description = "媒体文件管理接口",tags = {"媒体文件管理接口"})
public interface MediaFileControllerApi {
    @ApiOperation("查询文件列表")
    public QueryResponseResult findList(int page, int size, QueryMediaFileRequest queryMediaFileRequest);
}