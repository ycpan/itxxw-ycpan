package net.itxxw.api.cms;

import net.itxxw.framework.domain.system.SysDictionary;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author：ycpan
 * @date：Created in 2021/3/8 22:21
 * @description：系统字典控制器API接口
 * @modified By：
 * @version: $
 */
@Api(value = "功能等级的参数配置",description = "配置字典的取得")
public interface CmsSysDictionaryControllerApi {
    @ApiOperation("根据课程功能等级代码查询等级，代码200")
    public SysDictionary findSysDictionary(String type);
}
