package net.itxxw.api.cms;

import net.itxxw.framework.domain.cms.CmsPage;
import net.itxxw.framework.domain.cms.request.QueryPageRequest;
import net.itxxw.framework.domain.cms.response.CmsPageResult;
import net.itxxw.framework.domain.cms.response.CmsPostPageResult;
import net.itxxw.framework.model.response.QueryResponseResult;
import net.itxxw.framework.model.response.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import java.io.IOException;

@Api(value="cms页面管理接口",description = "cms页面管理接口，提供页面的增、删、改、查")
public interface CmsPageControllerApi {
    @ApiOperation("分页查询页面列表")
    @ApiImplicitParams({
     @ApiImplicitParam(name="page",value = "页码",required=true,paramType="path",dataType="int"),
          @ApiImplicitParam(name="size",value = "每页记录数",required=true,paramType="path",dataType="int")
                    })

    //页面查询
    public QueryResponseResult findCmsPageList(int page, int size, QueryPageRequest queryPageRequest);


    //新增页面
    @ApiOperation("新增页面")
    public CmsPageResult addCmsPage(CmsPage cmsPage);

    @ApiOperation("通过ID查询页面")
    public CmsPage findCmsPageById(String id);

    @ApiOperation("修改页面")
    public CmsPageResult updateCmsPage(String id,CmsPage cmsPage);

    @ApiOperation("页面保存")
    public CmsPageResult save(CmsPage cmsPage);

    //页面发布
    @ApiOperation("页面发布")
    public ResponseResult post( String pageId);

    @ApiOperation("一键发布页面")
    public CmsPostPageResult postPageQuick(CmsPage cmsPage);

}
