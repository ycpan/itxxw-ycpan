package net.itxxw.api.course;

import net.itxxw.framework.domain.course.CourseBase;
import net.itxxw.framework.domain.course.CourseMarket;
import net.itxxw.framework.domain.course.CoursePic;
import net.itxxw.framework.domain.course.Teachplan;
import net.itxxw.framework.domain.course.ext.CourseInfo;
import net.itxxw.framework.domain.course.ext.CourseView;
import net.itxxw.framework.domain.course.ext.TeachplanNode;
import net.itxxw.framework.domain.course.request.CourseListRequest;
import net.itxxw.framework.domain.course.response.AddCourseResult;
import net.itxxw.framework.domain.course.response.CoursePublishResult;
import net.itxxw.framework.model.response.QueryResponseResult;
import net.itxxw.framework.model.response.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @author：ycpan
 * @date：Created in 2020/12/7 21:35
 * @description：课程管理接口
 * @modified By：
 * @version: 1$
 */
@Api(value = "课程管理接口",description = "课程管理接口，提供课程的增/删/改/查")
public interface CourseControllerApi {

    @ApiOperation("添加课程计划")
    public ResponseResult addTeachplan(Teachplan teachplan);

    //查询课程列表
    @ApiOperation("分页查询课程列表")
    public QueryResponseResult findCourseList(int page, int size, CourseListRequest courseListRequest);

    @ApiOperation("查询指定公司下的所有课程")
    public QueryResponseResult<CourseInfo> findCourseListByCompany(
            int page,
            int size,
            CourseListRequest courseListRequest
    );

    @ApiOperation("新增课程基础信息")
    public AddCourseResult addCourseBase(CourseBase courseBase);

    @ApiOperation("单查课程的基础信息，用于回显")
    public CourseBase findCourseById(String courseid);

    @ApiOperation("更新课程基础信息")
    public ResponseResult updateCourseBase(String courseid,CourseBase courseBase);

    @ApiOperation("单查课程的营销信息，用于回显")
    public CourseMarket findMarketById(String courseid);

    @ApiOperation("更新课程营销信息")
    public ResponseResult updateMarket(String id,CourseMarket courseMarket);

    @ApiOperation("添加课程图片")
    public ResponseResult addCoursePic(String courseId,String pic);

    @ApiOperation("获取课程基础信息")
    public CoursePic findCoursePic(String courseId);

    @ApiOperation("删除课程图片")
    public ResponseResult deleteCoursePic(String courseId);

    @ApiOperation("通过课程id,取课程详情页面模板所需要的数据信息")
    public CourseView courseview(String courseId);

    @ApiOperation("通过课程id,取课程预览的接口")
    public CoursePublishResult preview(String id);

    @ApiOperation("通过课程id,一键发布课程的接口")
    public CoursePublishResult publish(String id);


}
