package net.itxxw.api.course;

/**
 * @author：ycpan
 * @date：Created in 2021/5/3 20:08
 * @description：
 * @modified By：
 * @version: $
 */

import net.itxxw.framework.domain.course.TeachplanMedia;
import net.itxxw.framework.domain.course.ext.TeachplanNode;
import net.itxxw.framework.model.response.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 课程计划管理相关的API接口
 */
@Api(value="课程计划管理API",description = "用于对课程计划的增删查改")
public interface TeachplanControllerApi {
    //课程计划相关操作
    @ApiOperation("课程计划查询")
    public TeachplanNode findTeachplanList(String courseId);

    @ApiOperation("保存媒资信息")
    public ResponseResult saveTeachplanMedia(TeachplanMedia teachplanMedia);
}
