package net.itxxw.filesystem.controller;

import net.itxxw.api.filesystem.FileSystemControllerApi;
import net.itxxw.filesystem.service.FileSystemService;
import net.itxxw.framework.domain.filesystem.response.UploadFileResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Administrator
 * @version 1.0
 **/
@RestController
@RequestMapping("/filesystem")
public class FileSystemController implements FileSystemControllerApi {
    @Autowired
    FileSystemService fileSystemService;


    @Override
    @PostMapping("/upload")
            //文件上传的controller,
            // @RequestParam("file") 前端传过来的文件名，必须为file
            //扩展：如果不要注解，那前端传过来的文件名，必须为multipartFile
            // 参数1：具体要上传的文件，//业务标签，//业务key,//文件元信息
    public UploadFileResult upload(@RequestParam("file") MultipartFile multipartFile, String filetag, String businesskey, String metadata) {

        return fileSystemService.upload(multipartFile, filetag, businesskey, metadata);
    }
}
