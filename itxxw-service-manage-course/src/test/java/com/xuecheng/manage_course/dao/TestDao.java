package net.itxxw.manage_course.dao;
import com.github.pagehelper.PageHelper;
import net.itxxw.framework.domain.course.CourseBase;
import net.itxxw.framework.domain.course.ext.CourseInfo;
import net.itxxw.framework.domain.course.ext.TeachplanNode;
import net.itxxw.framework.domain.course.request.CourseListRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

/**
 * @author Administrator
 * @version 1.0
 **/
@SpringBootTest
@RunWith(SpringRunner.class)
public class TestDao {
    @Autowired
    CourseBaseRepository courseBaseRepository;
    @Autowired
    CourseMapper courseMapper;

    @Autowired
    TeachplanMapper teachplanMapper;
    @Test
    public void testCourseBaseRepository(){
        Optional<CourseBase> optional = courseBaseRepository.findById("402885816243d2dd016243f24c030002");
        if(optional.isPresent()){
            CourseBase courseBase = optional.get();
            System.out.println(courseBase);
        }

    }


    @Test
    public void testCourseMapper(){
        CourseBase courseBase = courseMapper.findCourseBaseById("402885816240d276016240f7e5000002");
        System.out.println(courseBase);
    }



    @Test
    public void testFindTeachplan(){
        TeachplanNode teachplanNode = teachplanMapper.selectList("297e7c7c62b888f00162b8a7dec20000");
        System.out.println(teachplanNode);
    }

    //测试分页
    @Test
    public void testPageHelp(){
//        PageHelper.startPage(2,5);
//        CourseListRequest courseListRequest=new CourseListRequest();
//        Page<CourseInfo> courseList = courseMapper.findCourseList(courseListRequest);
//        List<CourseInfo> content = courseList.getContent();
//        System.out.println(content);
    }

}
