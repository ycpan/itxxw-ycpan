package net.itxxw.manage_course.client;

/**
 * @author：ycpan
 * @date：Created in 2021/3/26 15:05
 * @description：
 * @modified By：
 * @version: $
 */
import net.itxxw.framework.client.itxxwServiceList;
import net.itxxw.framework.domain.cms.CmsPage;
import net.itxxw.framework.domain.cms.response.CmsPageResult;
import net.itxxw.framework.domain.cms.response.CmsPostPageResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * cms服务的远程调用接口。再项目中可以，通过这个接口直接远程调用写好的方法
 */
@FeignClient(value =itxxwServiceList.ITXXW_SERVICE_MANAGE_CMS) //指定远程调用的服务名
public interface CmsPageClient {
    //根据页面id查询页面信息，远程调用cms请求数据
    //这里我在CmsPage接口定义的返回类型为 CmsPageResult 类型,所以远程调用接口这里也要接收 CmsPageResult 类型
    @GetMapping("/cms/page/get/{id}")//用GetMapping标识远程调用的http的方法类型
   public CmsPageResult findCmsPageById(@PathVariable("id") String id);

    //添加页面，用于课程预览
    @PostMapping("/cms/page/save")
   public CmsPageResult saveCmsPage(@RequestBody CmsPage cmsPage);

    //一键发布页面
    @PostMapping("/cms/page/postPageQuick")//指向cms服务中的发布方法
    public CmsPostPageResult postPageQuick(@RequestBody CmsPage cmsPage);
}
