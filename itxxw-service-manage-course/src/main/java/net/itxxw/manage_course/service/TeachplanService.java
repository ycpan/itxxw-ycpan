package net.itxxw.manage_course.service;

import net.itxxw.framework.domain.course.Teachplan;
import net.itxxw.framework.domain.course.TeachplanMedia;
import net.itxxw.framework.domain.course.ext.TeachplanNode;
import net.itxxw.framework.exception.ExceptionCast;
import net.itxxw.framework.model.response.CommonCode;
import net.itxxw.framework.model.response.ResponseResult;
import net.itxxw.manage_course.dao.CourseBaseRepository;
import net.itxxw.manage_course.dao.TeachplanMapper;
import net.itxxw.manage_course.dao.TeachplanMediaRepository;
import net.itxxw.manage_course.dao.TeachplanRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author：ycpan
 * @date：Created in 2021/5/3 20:15
 * @description：
 * @modified By：
 * @version: $
 */
@Service
public class TeachplanService {
    @Autowired
    TeachplanMapper teachplanMapper;

    @Autowired
    TeachplanMediaRepository teachplanMediaRepository;

    @Autowired
    TeachplanRepository teachplanRepository;

    @Autowired
    CourseBaseRepository courseBaseRepository;

    //保存媒资信息
    public ResponseResult saveTeachplanMedia(TeachplanMedia teachplanMedia) {
        if(teachplanMedia == null){
            ExceptionCast.cast(CommonCode.INVALID_PARAM);
        }

        //验证该课程计划是否存在
        String teachplanId = teachplanMedia.getTeachplanId();
        Optional<Teachplan> byId = teachplanRepository.findById(teachplanId);
        if(!byId.isPresent()){
            ExceptionCast.cast(CommonCode.COURSE_MEDIA_TEACHPLAN_ISNULL);
        }

        //只允许为第三级的课程,也就是叶子节点课程选择视频
        Teachplan teachplan = byId.get();
        String grade = teachplan.getGrade();
        if(StringUtils.isEmpty(grade) || !grade.equals("3")){
            ExceptionCast.cast(CommonCode.COURSE_MEDIA_TEACHPLAN_GRADEERROR);
        }

        //查询该课程计划下的媒资信息
        TeachplanMedia mediaOne = null;
        Optional<TeachplanMedia> mediaById = teachplanMediaRepository.findById(teachplanId);
        if(!mediaById.isPresent()){  //查询不到则新建一个的对象,如果查询到了就直接获取
            mediaOne = new TeachplanMedia();
        }else{
            mediaOne = mediaById.get();
        }

        //保存媒体信息与课程计划信息
        //BeanUtils.copyProperties(teachplanMedia,mediaOne);
        mediaOne.setCourseId(teachplanMedia.getCourseId());
        mediaOne.setTeachplanId(teachplanId);
        mediaOne.setMediaFileoriginalname(teachplanMedia.getMediaFileoriginalname());
        mediaOne.setMediaId(teachplanMedia.getMediaId());
        mediaOne.setMediaUrl(teachplanMedia.getMediaUrl());
        teachplanMediaRepository.save(mediaOne);
        return new ResponseResult(CommonCode.SUCCESS);
    }
    /**
     * 查询课程计划
     *
     * @param courseId
     * @return
     */
    public TeachplanNode findTeachplanList(String courseId) {
        TeachplanNode teachplanNode = teachplanMapper.selectList(courseId);
        return teachplanNode;
    }
}
