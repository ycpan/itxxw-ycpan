package net.itxxw.manage_course.dao;

import net.itxxw.framework.domain.course.CoursePub;
import org.springframework.data.jpa.repository.JpaRepository;

//课程索引的dao
public interface CoursePubRepository extends JpaRepository<CoursePub,String> {
}
