package net.itxxw.manage_course.dao;

import net.itxxw.framework.domain.course.CoursePic;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author：ycpan
 * @date：Created in 2021/3/25 22:44
 * @description：1
 * @modified By：
 * @version: 1$
 */
public interface CoursePicRepository extends JpaRepository<CoursePic,String> {

    //自定义一个通过courseId列删除，数据，成功返回受影响的行数
    Long deleteByCourseid(String courseId);
}
