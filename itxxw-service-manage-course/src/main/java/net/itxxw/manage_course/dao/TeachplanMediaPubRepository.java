package net.itxxw.manage_course.dao;

import net.itxxw.framework.domain.course.TeachplanMediaPub;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author：ycpan
 * @date：Created in 2021/5/14 21:20
 * @description：
 * @modified By：
 * @version: $
 */
public interface TeachplanMediaPubRepository extends JpaRepository<TeachplanMediaPub, String> {
    //根据课程id删除课程计划媒资信息
    long deleteByCourseId(String courseId);
}