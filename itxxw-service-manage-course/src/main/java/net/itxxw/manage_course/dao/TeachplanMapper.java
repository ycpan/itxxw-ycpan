package net.itxxw.manage_course.dao;

import net.itxxw.framework.domain.course.ext.TeachplanNode;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author：ycpan
 * @date：Created in 2020/12/13 13:23
 * @description：1
 * @modified By：
 * @version: 1$
 */
@Mapper
public interface TeachplanMapper {
    //课程计划_媒体查询
    public TeachplanNode selectList(String courseId);
}
