package net.itxxw.manage_course.dao;

import net.itxxw.framework.domain.course.TeachplanMedia;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


/**
 * @author：ycpan
 * @date：Created in 2021/5/3 20:16
 * @description：
 * @modified By：
 * @version: $
 */

//从TeachplanMedia查询课程计划媒资信息
public interface TeachplanMediaRepository extends JpaRepository<TeachplanMedia, String> {
    List<TeachplanMedia> findByCourseId(String courseId);
}
