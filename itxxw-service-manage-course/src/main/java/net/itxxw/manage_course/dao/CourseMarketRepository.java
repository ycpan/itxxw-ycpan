package net.itxxw.manage_course.dao;

import net.itxxw.framework.domain.course.CourseMarket;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author：ycpan
 * @date：Created in 2021/3/21 21:51
 * @description：课程营销dao层
 * @modified By：
 * @version: 1$
 */
public interface CourseMarketRepository  extends JpaRepository<CourseMarket,String> {

}
