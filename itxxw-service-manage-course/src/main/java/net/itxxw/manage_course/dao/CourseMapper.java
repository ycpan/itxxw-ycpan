package net.itxxw.manage_course.dao;

import com.github.pagehelper.Page;
import net.itxxw.framework.domain.course.CourseBase;
import net.itxxw.framework.domain.course.ext.CategoryNode;
import net.itxxw.framework.domain.course.ext.CourseInfo;
import net.itxxw.framework.domain.course.request.CourseListRequest;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by Administrator.
 */
@Mapper
public interface CourseMapper {
   //根据课程id查询课程信息
   CourseBase findCourseBaseById(String id);

   /**
    * 分页查询课程数据
    * @param courseListRequest 查询条件
    * @return
    */
   Page<CourseInfo> findCourseList(CourseListRequest courseListRequest);

   /**
    * 分页查询指定公司下的课程数据
    * @param courseListRequest 查询条件
    * @return
    */
   Page<CourseInfo> findCourseListByCompany(CourseListRequest courseListRequest);
}
