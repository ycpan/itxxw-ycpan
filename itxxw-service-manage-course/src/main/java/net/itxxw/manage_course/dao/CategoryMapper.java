package net.itxxw.manage_course.dao;

import net.itxxw.framework.domain.course.ext.CategoryNode;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author：ycpan
 * @date：Created in 2021/3/14 21:15
 * @description：课程分类查询接口
 * @modified By：
 * @version: 1$
 */
@Mapper
public interface CategoryMapper {
    //查询课程分类
    CategoryNode findCategoryList();
}
