package net.itxxw.manage_course.dao;

import net.itxxw.framework.domain.course.Teachplan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author：ycpan
 * @date：Created in 2020/12/13 19:53
 * @description：课程1级结点查询接口
 * @modified By：
 * @version: 1$
 */
public interface TeachplanRepository  extends JpaRepository<Teachplan,String> {
    //定义方法根据课程id和父结点id查询出结点列表，可以使用此方法实现查询根结点
    public List<Teachplan> findByCourseidAndParentid(String courseId, String parentId);
}
