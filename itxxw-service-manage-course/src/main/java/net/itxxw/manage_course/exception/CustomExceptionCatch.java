package net.itxxw.manage_course.exception;

import net.itxxw.framework.exception.ExceptionCatch;
import net.itxxw.framework.model.response.CommonCode;
import org.omg.CORBA.ExceptionList;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.nio.file.AccessDeniedException;

/**
 * @author：ycpan
 * @date：Created in 2021/6/2 12:30
 * @description：
 * @modified By：
 * @version: $
 */
@ControllerAdvice
public class CustomExceptionCatch extends ExceptionCatch {

    static {
        //除了CustomException以外的异常类型及对应的错误代码在这里定义,，如果不定义则统一返回固定的错误信息
        builder.put(AccessDeniedException.class, CommonCode.UNAUTHORISE);
    }
}
