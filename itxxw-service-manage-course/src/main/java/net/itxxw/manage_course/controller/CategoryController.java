package net.itxxw.manage_course.controller;

import net.itxxw.api.category.CategoryControllerApi;
import net.itxxw.framework.domain.course.ext.CategoryNode;
import net.itxxw.manage_course.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author：ycpan
 * @date：Created in 2021/3/8 21:55
 * @description：课程分类控制器
 * @modified By：
 * @version: 1$
 */
@RestController
@RequestMapping("/category")
public class CategoryController implements CategoryControllerApi {
    @Autowired
    private CourseService courseService;

    //查询课程分类
    @GetMapping("/list")
    public CategoryNode findCategoryList(){
        CategoryNode categoryList = courseService.findCategoryList();
        return categoryList;
    }

}
