package net.itxxw.manage_course.controller;

import net.itxxw.api.course.TeachplanControllerApi;
import net.itxxw.framework.domain.course.TeachplanMedia;
import net.itxxw.framework.domain.course.ext.TeachplanNode;
import net.itxxw.framework.model.response.ResponseResult;
import net.itxxw.manage_course.service.TeachplanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * @author：ycpan
 * @date：Created in 2021/5/3 20:12
 * @description：
 * @modified By：
 * @version: $
 */

@RestController
@RequestMapping("/course")
public class TeachplanController implements TeachplanControllerApi {
    @Autowired
    TeachplanService teachplanService;

    //@PreAuthorize("hasAuthority('course_teachplan_list')")
    @Override
    @GetMapping("/teachplan/list/{courseId}")
    public TeachplanNode findTeachplanList(@PathVariable("courseId") String courseId) {
        return teachplanService.findTeachplanList(courseId);
    }

    @Override
    @PostMapping("/savemedia")
    public ResponseResult saveTeachplanMedia(@RequestBody TeachplanMedia teachplanMedia) {
        return teachplanService.saveTeachplanMedia(teachplanMedia);
    }
}
