package net.itxxw.manage_course.controller;

        import net.itxxw.api.course.CourseControllerApi;
        import net.itxxw.framework.domain.course.CourseBase;
        import net.itxxw.framework.domain.course.CourseMarket;
        import net.itxxw.framework.domain.course.CoursePic;
        import net.itxxw.framework.domain.course.Teachplan;
        import net.itxxw.framework.domain.course.ext.CategoryNode;
        import net.itxxw.framework.domain.course.ext.CourseInfo;
        import net.itxxw.framework.domain.course.ext.CourseView;
        import net.itxxw.framework.domain.course.ext.TeachplanNode;
        import net.itxxw.framework.domain.course.request.CourseListRequest;
        import net.itxxw.framework.domain.course.response.AddCourseResult;
        import net.itxxw.framework.domain.course.response.CoursePublishResult;
        import net.itxxw.framework.exception.ExceptionCast;
        import net.itxxw.framework.model.response.CommonCode;
        import net.itxxw.framework.model.response.QueryResponseResult;
        import net.itxxw.framework.model.response.QueryResult;
        import net.itxxw.framework.model.response.ResponseResult;
        import net.itxxw.framework.utils.XcOauth2Util;
        import net.itxxw.framework.web.BaseController;
        import net.itxxw.manage_course.service.CourseService;
        import org.junit.runner.Request;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.security.access.prepost.PreAuthorize;
        import org.springframework.web.bind.annotation.*;
        import javax.servlet.http.HttpServletRequest;

/**
 * @author：ycpan
 * @date：Created in 2020/12/13 14:19
 * @description：课程接口查询
 * @modified By：
 * @version: 1$
 */
@RestController
@RequestMapping("/course")
public class CourseController extends BaseController implements CourseControllerApi {
    @Autowired
    CourseService courseService;

    @Override
    @PostMapping("/teachplan/add")
    public ResponseResult addTeachplan(@RequestBody Teachplan teachplan) {//@RequestBody 将JSON转为JAVA对象
        return courseService.addTeachplan(teachplan);
    }

    /**
     * 查询所有课程信息
     * @param page 页码
     * @param size 数量
     * @param courseListRequest 查询参数
     * @return QueryResponseResult
     */
    @Override
    @GetMapping("/coursebase/list/{page}/{size}")
    //查询课程列表,参数为：当前页码，每页显示条数，和一个备用的扩展参数
    @PreAuthorize("hasAuthority('course_find_list')")   //方法授权
    public QueryResponseResult findCourseList(@PathVariable("page") int page,@PathVariable("size") int size, CourseListRequest courseListRequest) {
        System.err.println(page+size);

        QueryResult<CourseInfo> courseList = courseService.findCourseList(page,size,courseListRequest);
        //封装分页数据集合和总记录数的对象
        QueryResult queryResult = new QueryResult();
        //得到分页数据集合
        queryResult.setList(courseList.getList());
        // 得到总记录数
        queryResult.setTotal(courseList.getTotal());
        QueryResponseResult queryResponseResult = new QueryResponseResult(CommonCode.SUCCESS,queryResult);
        return queryResponseResult;
    }

    /**
     * 查询指定公司下的所有课程
     * @param page 页码
     * @param size 数量
     * @param courseListRequest 查询参数
     * @return QueryResponseResult
     */
    @GetMapping("/coursebase/company/list/{page}/{size}")
    @Override
    public QueryResponseResult findCourseListByCompany(
            @PathVariable("page") int page,
            @PathVariable("size") int size,
            CourseListRequest courseListRequest
    ){
        //调用工具类取出用户信息
        XcOauth2Util xcOauth2Util = new XcOauth2Util();
        //从用户header中附带的jwt令牌取出用户信息
        XcOauth2Util.UserJwt userJwt = xcOauth2Util.getUserJwtFromHeader(request);
        //从用户信息获取该用户所属的公司id，根据该公司id查询该公司下的所有课程
        String companyId = userJwt.getCompanyId();
        return courseService.findCourseListByCompany(companyId, page, size, courseListRequest);
    }


    @Override
    @PostMapping("/coursebase/add")
    public AddCourseResult addCourseBase (@RequestBody CourseBase courseBase) {
        return  courseService.addCourseBase(courseBase);
    }

    //单查课程的基础信息，需要回显
    @Override
    @GetMapping("/coursebase/get/{id}")
    public CourseBase findCourseById(@PathVariable String id) {
        return courseService.findCourseById(id);
    }

    //修改课程基础信息
    @Override
    @PutMapping("/coursebase/update/{id}")
    public ResponseResult updateCourseBase(@PathVariable String id, @RequestBody CourseBase courseBase) {
        return courseService.updateCourse(id,courseBase);
    }

    //单查课程营销信息
    @Override
    @GetMapping("/coursemarket/get/{id}")
    public CourseMarket findMarketById(@PathVariable String id) {
        return courseService.findMarketById(id);
    }

    //课程营销修改
    @Override
    @PostMapping("/coursemarket/update/{id}")
    public ResponseResult updateMarket(@PathVariable String id, @RequestBody CourseMarket courseMarket) {
        return courseService.updateMarket(id,courseMarket);
    }


    //添加课程图片，会传进来一个对象，
    // 我们要获取对象中的值，用@RequestParam,注意，里面的参数名称要和前端封装的名称一致
    @Override
    @PostMapping("/coursepic/add")
    public ResponseResult addCoursePic(@RequestParam("courseId") String courseId,@RequestParam("pic") String pic) {
        return courseService.addCoursePic(courseId,pic);
    }

    //查询课程图片
    @PreAuthorize("hasAuthority('course_pic_list')")
    @Override
    @GetMapping("/coursepic/list/{id}")
    public CoursePic findCoursePic(@PathVariable String id) {
        return courseService.findCoursePic(id);
    }

    //删除图片，删除的id,是通过key,value对进来的，
    //  注解取参数的时候，需要用@RequestParam("key")取值
    @PreAuthorize("hasAuthority('course_find_list')")   //方法授权
    @Override
    @DeleteMapping("/coursepic/delete")
    public ResponseResult deleteCoursePic(@RequestParam("courseId") String courseId) {
        return courseService.deleteCoursePic(courseId);
    }

    //查询课程详情页面的数据组合
    @Override
    @GetMapping("/courseview/{id}")
    public CourseView courseview(@PathVariable("id") String courseId) {
        return courseService.getCourseView(courseId);
    }

    //通过课程id,取课程预览的接口
    @Override
    @PostMapping("/preview/{id}")
    public CoursePublishResult preview(@PathVariable("id") String id) {//id为课程id
        return courseService.preview(id);
    }

    //课程发布
    @Override
    @PostMapping("/publish/{id}")
    public CoursePublishResult publish(@PathVariable("id") String id) {
        return courseService.publish(id);
    }



}
