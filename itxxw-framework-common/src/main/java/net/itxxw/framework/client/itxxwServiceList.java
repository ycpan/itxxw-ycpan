package net.itxxw.framework.client;


public class itxxwServiceList {
    public static final String ITXXW_GOVERN_CENTER = "itxxw-govern-center";
    public static final String ITXXW_SERVICE_PORTALVIEW = "itxxw-service-portalview";
    public static final String ITXXW_SERVICE_SEARCH = "itxxw-service-search";
    public static final String ITXXW_SERVICE_MANAGE_COURSE = "itxxw-service-manage-course";
    public static final String ITXXW_SERVICE_MANAGE_MEDIA = "itxxw-service-manage-media";
    public static final String ITXXW_SERVICE_MANAGE_CMS = "itxxw-service-manage-cms";
    public static final String ITXXW_SERVICE_UCENTER = "itxxw-service-ucenter";
    public static final String ITXXW_SERVICE_UCENTER_AUTH = "itxxw-service-ucenter-auth";
    public static final String ITXXW_SERVICE_UCENTER_JWT = "itxxw-service-ucenter-jwt";
    public static final String ITXXW_SERVICE_BASE_FILESYSTEM = "itxxw-service-base-filesystem";
    public static final String ITXXW_GOVERN_GATEWAY = "itxxw-govern-gateway";
    public static final String ITXXW_SERVICE_BASE_ID = "itxxw-service-base-id";
    public static final String ITXXW_SERVICE_MANAGE_ORDER = "itxxw-service-manage-order";
    public static final String ITXXW_SERVICE_LEARNING = "itxxw-service-learning";

}
