package net.itxxw.order.mq;

import com.rabbitmq.client.Channel;
import net.itxxw.framework.domain.task.XcTask;
import net.itxxw.order.config.RabbitMQConfig;
import net.itxxw.order.service.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * @author：ycpan
 * @date：Created in 2021/6/11 20:16
 * @description：
 * @modified By：
 * @version: $
 */
@Component
public class ChooseCourseTask {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChooseCourseTask.class);
    @Autowired
    TaskService taskService;
    //每隔1分钟扫描消息表，向mq发送消息
    @Scheduled(cron = "0/60 * * * * *")
    public void sendChoosecourseTask() {
        //取出当前时间1分钟之前的时间
        //Calendar类是个抽象类 ,为我们提供了 关于日期计算的相关功能,比如:年、 月、日、时、分、秒的展示和计算。
        //GregorianCalendar是Calendar的一个具体子类，提供了世界上大多数国家/地区使用的标准日历系统。
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        /*
        * 1代表的是对年份操作，
        2是对月份操作，
        3是对星期操作，
        5是对日期操作，
        11是对小时操作，
        12是对分钟操作，
        13是对秒操作，
        14是对毫秒操作。
        举例：
        Calendar calendar = Calendar.getInstance();
        calendar.add(2, 1);//表示对月进行加一天操作
        calendar.add(5, -1);//表示对日期进行减一天操作
        calendar.add(5, 1);//表示对日期进行加一天操作
     * */
        calendar.add(GregorianCalendar.MINUTE, -1);
        Date time = calendar.getTime();
        List<XcTask> taskList = taskService.findTaskList(time, 10);

        //遍历任务列表
        for (XcTask xcTask : taskList) {
            //任务id
            String taskId = xcTask.getId();
            //版本号
            Integer version = xcTask.getVersion();
            //调用乐观锁方法校验任务是否可以执行
            if (taskService.getTask(taskId,version)>0) {
                //发送选课消息
                taskService.publish(xcTask,xcTask.getMqExchange(), xcTask.getMqRoutingkey());
                LOGGER.info("send choose course task id:{}", taskId);
            }
        }
    }
    /**
     * 接收选课响应结果
     */
    @RabbitListener(queues = {RabbitMQConfig.XC_LEARNING_FINISHADDCHOOSECOURSE})
    public void receiveFinishChoosecourseTask(XcTask task, Message message, Channel channel) throws
            IOException {
        LOGGER.info("receiveChoosecourseTask...{}",task.getId());
         //接收到 的消息id
        String id = task.getId();
         //删除任务，添加历史任务
        taskService.finishTask(id);
    }

}
