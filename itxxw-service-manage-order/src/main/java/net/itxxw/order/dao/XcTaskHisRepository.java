package net.itxxw.order.dao;

/**
 * @author：ycpan
 * @date：Created in 2021/6/11 21:30
 * @description：
 * @modified By：
 * @version: $
 */
import net.itxxw.framework.domain.task.XcTaskHis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface XcTaskHisRepository extends JpaRepository<XcTaskHis, String> {

}