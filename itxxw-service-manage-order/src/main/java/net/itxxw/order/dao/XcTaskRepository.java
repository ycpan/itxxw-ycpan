package net.itxxw.order.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import net.itxxw.framework.domain.task.XcTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;

/**
 * @author：ycpan
 * @date：Created in 2021/6/11 20:03
 * @description：查询前N条任务
 * @modified By：
 * @version: $
 */

public interface XcTaskRepository extends JpaRepository<XcTask, String> {
    //取出指定时间之前的记录
    //findByUpdateTimeBefore 方法为 JPA 自带的一个组合方法，可以根据传入的分页参数以及时间查询，查询到该时间之前的数据。
    Page<XcTask> findByUpdateTimeBefore(Pageable pageable, Date updateTime);

    //更新任务处理时间
    @Modifying
    @Query("update XcTask t set t.updateTime = :updateTime where t.id = :id ")
    public int updateTaskTime(@Param(value = "id") String id, @Param(value = "updateTime")Date
            updateTime);

    //使用乐观锁方式校验任务id和版本号是否匹配，匹配则版本号加1
    @Modifying
    @Query("update XcTask t set t.version = :version+1 where t.id = :id and t.version = :version")
            public int updateTaskVersion(@Param(value = "id") String id,@Param(value = "version") int
            version);

}